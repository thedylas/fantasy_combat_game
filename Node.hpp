/*********************************************************************
 ** Program name: project3
 ** Author: David Anderson
 ** Date: 02/10/2019
 ** Description: This program is fantasy combat game
 *********************************************************************/

#ifndef CS162_LAB6_NODE_HPP
#define CS162_LAB6_NODE_HPP

#include "Character.hpp"

class Node {
public:
    Node() = default;

    ~Node();


    //get functions
    Node *getNext();


    Node *getPrev();


    Character *getCharacter();


    //set functions
    void setNext(Node *);


    void setPrev(Node *);


    void setCharacter(Character **);


private:
    Node *next_;         //pointer to next node
    Node *prev_{};         //pointer to previous node
    Character *character_;            //value of node
};


#endif //CS162_LAB6_NODE_HPP
