/*********************************************************************
 ** Program name: project3
 ** Author: David Anderson
 ** Date: 02/10/2019
 ** Description: This program is fantasy combat game
 *********************************************************************/

#ifndef PROJECT3_CHARACTER_HPP
#define PROJECT3_CHARACTER_HPP

#include <iostream>

class Character {
public:
    //constructor
    Character();


    //destructor
    virtual ~Character();


    //functions inherited by child classes
    virtual int attackFunction() = 0;


    virtual int defenseFunction(int) = 0;


    //get functions
    int getArmor();


    int getStrengthPoints();


    std::string getDisplayType();


    std::string getCharacterName();


    //set functions
    void setAttackNumberOfDie(int);


    virtual void setAttackDieSides(int);


    void setDefenseNumberOfDie(int);


    void setDefenseDieSides(int);


    void setArmor(int);


    void setStrengthPoints(int);


    void setDisplayType(std::string);


    void setCharacterName(std::string);


    void recover();


private:
    int attackNumberOfDie_;
    int attackDieSides_;
    int defenseNumberOfDie_;
    int defenseDieSides_;
    int armor_;
    int strengthPoints_;
    std::string displayType_;
    std::string characterName_;
};

#endif //PROJECT3_CHARACTER_HPP
