/*********************************************************************
 ** Program name: project3
 ** Author: David Anderson
 ** Date: 02/10/2019
 ** Description: This program is fantasy combat game
 *********************************************************************/

#include "Vampire.hpp"

//default attack function
int Vampire::attackFunction() {
    int attackRoll = 0;

    for (int i = 1; i <= this->attackNumberOfDie_; i++) {
        attackRoll += (rand() % this->attackDieSides_) + 1;
    }

    return attackRoll;
}

//defense function with special ability
int Vampire::defenseFunction(int attackRoll) {
    int defenseRoll = 0;
    int damageInflicted = 0;
    int flip;

    //charm special ability
    flip = (rand() % 2) + 1;
    if (flip == 1) {
        attackRoll = 0;
        /*debug
        std::cout << "SPECIAL ABILITY USED: attack roll now = " << attackRoll << "\n";
         */
    }

    for (int i = 1; i <= this->defenseNumberOfDie_; i++) {
        defenseRoll += (rand() % this->defenseDieSides_) + 1;
    }

    /*debug
    std::cout << "Defense dice roll: " << defenseRoll << "\n";
     */

    damageInflicted = attackRoll - defenseRoll - this->armor_;
    if (damageInflicted > 0 && this->strengthPoints_ > 0) {
        setStrengthPoints(((getStrengthPoints()) - damageInflicted));
    }

    return damageInflicted;
}

