/*********************************************************************
 ** Program name: project3
 ** Author: David Anderson
 ** Date: 02/10/2019
 ** Description: This program is fantasy combat game
 *********************************************************************/

#ifndef DEFAULTFUNCTIONS_MAINMENU_HPP
#define DEFAULTFUNCTIONS_MAINMENU_HPP

#include "Character.hpp"
#include "Barbarian.hpp"
#include "Blue_Men.hpp"
#include "Harry_Potter.hpp"
#include "Medusa.hpp"
#include "Vampire.hpp"
#include "LinkedList.hpp"

enum turnResult {
    TEAM1WON, TEAM2WON, INPROGRESS
};

class menu {
public:
    //constructor
    menu() = default;


    //primary menu to initiate the game
    void useMainMenu();


    //secondary menu to restart the game once it has been played
    void useSecondaryMenu();


    //input validation
    bool verifyYN(std::string);


    bool verifyFighterNumber(int);


    //allows the user to choose characters
    Character *chooseCharacter1(std::string);


    Character *chooseCharacter2(std::string);


    //initiates the turn function
    void combat(LinkedList *, LinkedList *);


    //each turn has attacks and defense functions used
    turnResult turn(LinkedList *, LinkedList *, LinkedList *, int);


    bool getIsQuitGame();


private:
    bool isMenuQuit_{};
    bool isGoodInput_{};
    bool isQuitGame_{};
    int choiceInt_{};
    int team1NumberOfFighters_;
    int team2NumberOfFighters_;
    std::string userInputString_;
    Character *character1_{};
    Character *character2_{};
};

#endif //DEFAULTFUNCTIONS_MAINMENU_HPP
