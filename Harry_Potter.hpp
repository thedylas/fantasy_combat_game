/*********************************************************************
 ** Program name: project3
 ** Author: David Anderson
 ** Date: 02/10/2019
 ** Description: This program is fantasy combat game
 *********************************************************************/

#ifndef PROJECT3_HARRY_POTTER_HPP
#define PROJECT3_HARRY_POTTER_HPP

#include "Character.hpp"

class Harry_Potter : public Character {
public:
    //constructor
    Harry_Potter() = default;


    //functions inherited from parent class
    int attackFunction();


    int defenseFunction(int);

private:
    int attackNumberOfDie_ = 2;
    int attackDieSides_ = 6;
    int defenseNumberOfDie_ = 2;
    int defenseDieSides_ = 6;
    int armor_ = 0;
    int strengthPoints_ = 10;
    bool died;
    std::string characterName_;
};


#endif //PROJECT3_HARRY_POTTER_HPP
