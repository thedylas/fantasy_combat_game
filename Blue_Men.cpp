/*********************************************************************
 ** Program name: project3
 ** Author: David Anderson
 ** Date: 02/10/2019
 ** Description: This program is fantasy combat game
 *********************************************************************/

#include "Blue_Men.hpp"

//default attack function
int Blue_Men::attackFunction() {
    int attackRoll = 0;

    for (int i = 1; i <= this->attackNumberOfDie_; i++) {
        attackRoll += (rand() % this->attackDieSides_) + 1;
    }

    return attackRoll;
}

//defense function with special ability
int Blue_Men::defenseFunction(int attackRoll) {
    int defenseRoll = 0;
    int damageInflicted = 0;
    int i = 0;
    int count = 0;

    //mob special ability
    while (i < attackRoll) {
        i += 4;
        if (i > attackRoll) {
            break;
        } else { count++; }
    }

    this->defenseNumberOfDie_ -= count;

    /*debug
    if (count > 0) {
        std::cout << "SPECIAL ABILITY USED: defense die reduced by: " << count << "\n";
    }
     */

    for (i = 1; i <= this->defenseNumberOfDie_; i++) {
        defenseRoll += (rand() % this->defenseDieSides_) + 1;
    }

    /*debug
    std::cout << "Defense dice roll: " << defenseRoll << "\n";
    */

    damageInflicted = attackRoll - defenseRoll - this->armor_;
    if (damageInflicted > 0 && this->strengthPoints_ > 0) {
        setStrengthPoints(((getStrengthPoints()) - damageInflicted));
    }

    this->defenseNumberOfDie_ = 3;

    return damageInflicted;
}