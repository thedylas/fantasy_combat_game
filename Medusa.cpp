/*********************************************************************
 ** Program name: project3
 ** Author: David Anderson
 ** Date: 02/10/2019
 ** Description: This program is fantasy combat game
 *********************************************************************/

#include "Medusa.hpp"

//attack function with special ability
int Medusa::attackFunction() {
    int attackRoll = 0;

    for (int i = 1; i <= this->attackNumberOfDie_; i++) {
        attackRoll += (rand() % this->attackDieSides_) + 1;
    }

    //glare special ability
    if (attackRoll == 12) {
        attackRoll = 100;
    }

    return attackRoll;
}

//default defense function
int Medusa::defenseFunction(int attackRoll) {
    int defenseRoll = 0;
    int damageInflicted = 0;

    for (int i = 1; i <= this->defenseNumberOfDie_; i++) {
        defenseRoll += (rand() % this->defenseDieSides_) + 1;
    }

    /*debug
    std::cout << "Defense dice roll: " << defenseRoll << "\n";
    */

    damageInflicted = attackRoll - defenseRoll - this->armor_;
    if (damageInflicted > 0 && this->strengthPoints_ > 0) {
        setStrengthPoints(((getStrengthPoints()) - damageInflicted));
    }

    return damageInflicted;
}

