/*********************************************************************
 ** Program name: project3
 ** Author: David Anderson
 ** Date: 02/10/2019
 ** Description: This program is fantasy combat game
 *********************************************************************/

#include "Barbarian.hpp"

//default attack function
int Barbarian::attackFunction() {
    int attackRoll = 0;

    for (int i = 1; i <= this->attackNumberOfDie_; i++) {
        attackRoll += (rand() % this->attackDieSides_) + 1;
    }

    return attackRoll;
}

//default defense function
int Barbarian::defenseFunction(int attackRoll) {
    int defenseRoll = 0;
    int damageInflicted = 0;

    for (int i = 1; i <= this->defenseNumberOfDie_; i++) {
        defenseRoll += (rand() % this->defenseDieSides_) + 1;
    }

    /*
    std::cout << "Defense dice roll: " << defenseRoll << "\n";
    */

    damageInflicted = attackRoll - defenseRoll - this->armor_;
    if (damageInflicted > 0 && this->strengthPoints_ > 0) {
        setStrengthPoints(((getStrengthPoints()) - damageInflicted));
    }

    return damageInflicted;
}