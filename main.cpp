/*********************************************************************
 ** Program name: project3
 ** Author: David Anderson
 ** Date: 02/10/2019
 ** Description: This program is fantasy combat game
 *********************************************************************/

#include <iostream>
#include <ctime>
#include <limits>

#include "menu.hpp"

int main() {
    //seed rand
    std::srand(time(nullptr));

    menu menu1;

    if (!menu1.getIsQuitGame()) {
        int initialChoice;
        bool isGoodInput = false;
        while (!isGoodInput) {
            std::cout << "Choose one: \n"
                      << "1. Play\n"
                      << "2. Exit\n";

            std::cin >> initialChoice;

            //incorrect answer
            if ((initialChoice < 1 || initialChoice > 2 || std::cin.fail())) {
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                std::cout << std::endl;
                isGoodInput = false;
                std::cout << "Please enter a valid input\n";
            }

            //correct answer
            if (initialChoice >= 1 && initialChoice <= 2) {
                isGoodInput = true;
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }

        }

        switch (initialChoice) {
            case 1: {
                std::cout << "Starting game...\n";
                break;
            }

            case 2: {
                std::cout << "Goodbye\n";
                return 0;
            }
        }

        //first play through
        menu1.useMainMenu();

        while (!menu1.getIsQuitGame()) {
            //subsequent play through(s)
            menu1.useSecondaryMenu();
        }
    }

    return 0;
}