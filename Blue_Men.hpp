/*********************************************************************
 ** Program name: project3
 ** Author: David Anderson
 ** Date: 02/10/2019
 ** Description: This program is fantasy combat game
 *********************************************************************/

#ifndef PROJECT3_BLUE_MEN_HPP
#define PROJECT3_BLUE_MEN_HPP

#include "Character.hpp"

class Blue_Men : public Character {
public:
    //constructor
    Blue_Men() = default;


    //functions inherited from the parent class
    int attackFunction();


    int defenseFunction(int);


private:
    int attackNumberOfDie_ = 2;
    int attackDieSides_ = 10;
    int defenseNumberOfDie_ = 3;
    int defenseDieSides_ = 6;
    int armor_ = 3;
    int strengthPoints_ = 12;
    std::string characterName_;
};


#endif //PROJECT3_BLUE_MEN_HPP
