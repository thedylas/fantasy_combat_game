/*********************************************************************
 ** Program name: project3
 ** Author: David Anderson
 ** Date: 02/10/2019
 ** Description: This program is fantasy combat game
 *********************************************************************/

#ifndef PROJECT3_VAMPIRE_HPP
#define PROJECT3_VAMPIRE_HPP

#include "Character.hpp"

class Vampire : public Character {
public:
    //constructor
    Vampire() = default;


    //functions inherited from parent class
    int attackFunction();


    int defenseFunction(int) override;


private:
    int attackNumberOfDie_ = 1;
    int attackDieSides_ = 12;
    int defenseNumberOfDie_ = 1;
    int defenseDieSides_ = 6;
    int armor_ = 1;
    int strengthPoints_ = 18;
    std::string characterName_;
};


#endif //PROJECT3_VAMPIRE_HPP
