/*********************************************************************
 ** Program name: project3
 ** Author: David Anderson
 ** Date: 02/10/2019
 ** Description: This program is fantasy combat game
 *********************************************************************/

#include "Harry_Potter.hpp"

//default attack function
int Harry_Potter::attackFunction() {
    int attackRoll = 0;

    for (int i = 1; i <= this->attackNumberOfDie_; i++) {
        attackRoll += (rand() % this->attackDieSides_) + 1;
    }

    return attackRoll;
}

//defense function with special ability
int Harry_Potter::defenseFunction(int attackRoll) {
    int defenseRoll = 0;
    int damageInflicted = 0;

    for (int i = 1; i <= this->defenseNumberOfDie_; i++) {
        defenseRoll += (rand() % this->defenseDieSides_) + 1;
    }

    /*debug
    std::cout << "Defense dice roll: " << defenseRoll << "\n";
    */

    damageInflicted = attackRoll - defenseRoll - this->armor_;

    if (damageInflicted > 0 && this->strengthPoints_ > 0) {
        setStrengthPoints(((getStrengthPoints()) - damageInflicted));

        //Hogwarts special ability
        if (getStrengthPoints() <= 0 && !died) {
            died = true;
            /*debug
            std::cout << "SPECIAL ABILITY USED: Revived = " << died << "\n";
            */
            setStrengthPoints(20);
            damageInflicted = 0;
        }
    }
    return damageInflicted;
}

