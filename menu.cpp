/*********************************************************************
 ** Program name: project3
 ** Author: David Anderson
 ** Date: 02/10/2019
 ** Description: This program is fantasy combat game
 *********************************************************************/

#include <iostream>
#include <limits>
#include <string>
#include "menu.hpp"
#include "LinkedList.hpp"

bool menu::verifyFighterNumber(int userInputInt) {
    if (userInputInt < 1 || userInputInt > 20) {
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cout << std::endl;
        isGoodInput_ = false;
        std::cout << "Please enter a valid input\n";
        return false;
    } else { return true; }
}

void menu::useMainMenu() {
    LinkedList team1;
    LinkedList team2;

    isGoodInput_ = false;
    while (!isGoodInput_) {
        //tells the user to pick an option
        std::cout << "How many fighters should team 1 have?\n";

        //takes in the option that the user chooses
        std::cin >> team1NumberOfFighters_;

        //input validation
        isGoodInput_ = verifyFighterNumber(team1NumberOfFighters_);
    }
    isGoodInput_ = false;
    while (!isGoodInput_) {
        //tells the user to pick an option
        std::cout << "How many fighters should team 2 have?\n";

        //takes in the option that the user chooses
        std::cin >> team2NumberOfFighters_;

        //input validation
        isGoodInput_ = verifyFighterNumber(team2NumberOfFighters_);
    }

    //the user chooses which characters they want to fight
    for (int i = 0; i < team1NumberOfFighters_; i++) {
        std::string characterName;
        std::cout << "Name a fighter for team 1: \n";

        std::getline(std::cin >> std::ws, characterName);
        std::cin.clear();

        std::cout << "Choose a fighter for team 1: \n";
        Character **tempCharacter = new Character *;
        *tempCharacter = chooseCharacter1(characterName);

        team1.addNodeToTail(tempCharacter);

        std::cout << "You have " << team1NumberOfFighters_ - i - 1 << " fighters left to choose for team 1.\n\n";
    }

    for (int i = 0; i < team2NumberOfFighters_; i++) {
        std::string characterName;
        std::cout << "Name a fighter for team 2: \n";

        std::getline(std::cin >> std::ws, characterName);
        std::cin.clear();

        std::cout << "Choose a fighter for team 2: \n";
        Character **tempCharacter = new Character *;
        *tempCharacter = chooseCharacter2(characterName);

        team2.addNodeToTail(tempCharacter);

        std::cout << "You have " << team2NumberOfFighters_ - i - 1 << " fighters left to choose for team 2.\n\n";
    }
    //the simulation is ran
    combat(&team1, &team2);
}

//if the user indicates that they want to play again, the main menu is called, otherwise it quits the game
void menu::useSecondaryMenu() {
    int userInput;
    isGoodInput_ = false;
    while (!isGoodInput_) {
        //tells the user to pick an option
        std::cout << "What would you like to do now?\n"
                  << "1. Play again\n"
                  << "2. Exit\n";

        std::cin >> userInput;


        //incorrect answer
        if ((userInput < 1 || userInput > 2 || std::cin.fail())) {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            std::cout << std::endl;
            isGoodInput_ = false;
            std::cout << "Please enter a valid input\n";
            break;
        }

        //correct answer
        if (userInput >= 1 && userInput <= 2) {
            isGoodInput_ = true;
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            break;
        }
    }
    switch (userInput) {
        case 1: {
            useMainMenu();
            break;
        }
        case 2: {
            isMenuQuit_ = true;
            isQuitGame_ = true;
            break;
        }
    }
}

//string input validation
bool menu::verifyYN(std::string) {
    if (userInputString_ != "y" && userInputString_ != "n") {
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cout << std::endl;
        isGoodInput_ = false;
        std::cout << "Please enter a valid input\n";
        return false;
    } else { return true; }
}

bool menu::getIsQuitGame() {
    return isQuitGame_;
}

Character *menu::chooseCharacter1(std::string characterName) {
    isGoodInput_ = false;
    isMenuQuit_ = false;
    while (!isMenuQuit_) {
        while (!isGoodInput_) {
            //tells the user to pick an option
            std::cout << "Choose a character: \n"
                      << "1. Vampire\n"
                      << "2. Barbarian\n"
                      << "3. Blue Men\n"
                      << "4. Medusa\n"
                      << "5. Harry Potter\n";

            //takes in the option that the user chooses
            std::cin >> choiceInt_;

            //incorrect answer
            if ((choiceInt_ < 1 || choiceInt_ > 5 || std::cin.fail())) {
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                std::cout << std::endl;
                isGoodInput_ = false;
                std::cout << "Please enter a valid input\n";
                break;
            }

            //correct answer
            if (choiceInt_ >= 1 && choiceInt_ <= 5) {
                isGoodInput_ = true;
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                break;
            }
        }
        switch (choiceInt_) {
            //user chose vampire
            case 1: {
                character1_ = new Vampire;
                character1_->setAttackNumberOfDie(1);
                character1_->setAttackDieSides(12);
                character1_->setDefenseNumberOfDie(1);
                character1_->setDefenseDieSides(6);
                character1_->setArmor(1);
                character1_->setStrengthPoints(18);
                character1_->setDisplayType("Vampire");
                character1_->setCharacterName(characterName);

                isMenuQuit_ = true;
                return character1_;
            }

            case 2: {
                //user chose barbarian
                character1_ = new Barbarian;
                character1_->setAttackNumberOfDie(2);
                character1_->setAttackDieSides(6);
                character1_->setDefenseNumberOfDie(2);
                character1_->setDefenseDieSides(6);
                character1_->setArmor(0);
                character1_->setStrengthPoints(12);
                character1_->setDisplayType("Barbarian");
                character1_->setCharacterName(characterName);

                isMenuQuit_ = true;
                return character1_;
            }

            case 3: {
                //user chose blue men
                character1_ = new Blue_Men;
                character1_->setAttackNumberOfDie(2);
                character1_->setAttackDieSides(10);
                character1_->setDefenseNumberOfDie(3);
                character1_->setDefenseDieSides(6);
                character1_->setArmor(3);
                character1_->setStrengthPoints(12);
                character1_->setDisplayType("Blue Men");
                character1_->setCharacterName(characterName);

                isMenuQuit_ = true;
                return character1_;
            }

            case 4: {
                //user chose medusa
                character1_ = new Medusa;
                character1_->setAttackNumberOfDie(2);
                character1_->setAttackDieSides(6);
                character1_->setDefenseNumberOfDie(1);
                character1_->setDefenseDieSides(6);
                character1_->setArmor(3);
                character1_->setStrengthPoints(8);
                character1_->setDisplayType("Medusa");
                character1_->setCharacterName(characterName);

                isMenuQuit_ = true;
                return character1_;
            }

            case 5: {
                //user chose harry potter
                character1_ = new Harry_Potter;
                character1_->setAttackNumberOfDie(2);
                character1_->setAttackDieSides(6);
                character1_->setDefenseNumberOfDie(2);
                character1_->setDefenseDieSides(6);
                character1_->setArmor(0);
                character1_->setStrengthPoints(10);
                character1_->setDisplayType("Harry Potter");
                character1_->setCharacterName(characterName);

                isMenuQuit_ = true;
                return character1_;
            }
            default:
                break;
        }
    }
    return character1_;
}

Character *menu::chooseCharacter2(std::string characterName) {
    isGoodInput_ = false;
    isMenuQuit_ = false;
    while (!isMenuQuit_) {
        while (!isGoodInput_) {
            //tells the user to pick an option
            std::cout << "Choose a character: \n"
                      << "1. Vampire\n"
                      << "2. Barbarian\n"
                      << "3. Blue Men\n"
                      << "4. Medusa\n"
                      << "5. Harry Potter\n";

            //takes in the option that the user chooses
            std::cin >> choiceInt_;

            //incorrect answer
            if ((choiceInt_ < 1 || choiceInt_ > 5 || std::cin.fail())) {
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                std::cout << std::endl;
                isGoodInput_ = false;
                std::cout << "Please enter a valid input\n";
                break;
            }

            //correct answer
            if (choiceInt_ >= 1 && choiceInt_ <= 5) {
                isGoodInput_ = true;
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                break;
            }
        }
        switch (choiceInt_) {
            case 1: {
                //user chose vampire
                character2_ = new Vampire;
                character2_->setAttackNumberOfDie(1);
                character2_->setAttackDieSides(12);
                character2_->setDefenseNumberOfDie(1);
                character2_->setDefenseDieSides(6);
                character2_->setArmor(1);
                character2_->setStrengthPoints(18);
                character2_->setDisplayType("Vampire");
                character2_->setCharacterName(characterName);

                isMenuQuit_ = true;
                return character2_;
            }

            case 2: {
                //user chose barbarian
                character2_ = new Barbarian;
                character2_->setAttackNumberOfDie(2);
                character2_->setAttackDieSides(6);
                character2_->setDefenseNumberOfDie(2);
                character2_->setDefenseDieSides(6);
                character2_->setArmor(0);
                character2_->setStrengthPoints(12);
                character2_->setDisplayType("Barbarian");
                character2_->setCharacterName(characterName);

                isMenuQuit_ = true;
                return character2_;
            }

            case 3: {
                //user chose blue men
                character2_ = new Blue_Men;
                character2_->setAttackNumberOfDie(2);
                character2_->setAttackDieSides(10);
                character2_->setDefenseNumberOfDie(3);
                character2_->setDefenseDieSides(6);
                character2_->setArmor(3);
                character2_->setStrengthPoints(12);
                character2_->setDisplayType("Blue Men");
                character2_->setCharacterName(characterName);

                isMenuQuit_ = true;
                return character2_;
            }

            case 4: {
                //user chose medusa
                character2_ = new Medusa;
                character2_->setAttackNumberOfDie(2);
                character2_->setAttackDieSides(6);
                character2_->setDefenseNumberOfDie(1);
                character2_->setDefenseDieSides(6);
                character2_->setArmor(3);
                character2_->setStrengthPoints(8);
                character2_->setDisplayType("Medusa");
                character2_->setCharacterName(characterName);

                isMenuQuit_ = true;
                return character2_;
            }

            case 5: {
                //user chose harry potter
                character2_ = new Harry_Potter;
                character2_->setAttackNumberOfDie(2);
                character2_->setAttackDieSides(6);
                character2_->setDefenseNumberOfDie(2);
                character2_->setDefenseDieSides(6);
                character2_->setArmor(0);
                character2_->setStrengthPoints(10);
                character2_->setDisplayType("Harry Potter");
                character2_->setCharacterName(characterName);

                isMenuQuit_ = true;
                return character2_;
            }
            default:
                break;
        }
    }
    return character2_;
}


void menu::combat(LinkedList *team1, LinkedList *team2) {
    int round = 1;
    int team1Score = 0;
    int team2Score = 0;
    LinkedList dead;

    //runs turns until the function returns false because a character died
    turnResult resultHolder;
    while (team1->getNextCharacter() != nullptr && team2->getNextCharacter() != nullptr) {
        while ((resultHolder = turn(team1, team2, &dead, round)) == INPROGRESS) {
        }

        //whichever player still had strength points remaining wins
        if (resultHolder == TEAM1WON) {
            /*debug
            std::cout << "\n*************************************************\n";
            std::cout << "********** CHARACTER 1 (" << team1->getNextCharacter()->getDisplayType()
                      << ") WINS! **********\n";
            std::cout << "*************************************************\n\n";
            std::cout << "The dead: ";
            dead.printHeadToTail();
             */
            team1->getNextCharacter()->recover();
            round++;
            team1Score++;
        }
        if (resultHolder == TEAM2WON) {
            /*debug
            std::cout << "\n*************************************************\n";
            std::cout << "********** CHARACTER 2 (" << team2->getNextCharacter()->getDisplayType()
                      << ") WINS! **********\n";
            std::cout << "*************************************************\n\n";
            std::cout << "The dead: ";
            dead.printHeadToTail();
             */
            team2->getNextCharacter()->recover();
            round++;
            team2Score++;
        }
    }

    std::cout << "Team 1 score: " << team1Score << "\n";
    std::cout << "Team 2 score: " << team2Score << "\n";

    if (team1Score > team2Score) {
        std::cout << "Team 1 wins!!\n";
    }
    if (team2Score > team1Score) {
        std::cout << "Team 2 wins!!\n";
    }
    if (team1Score == team2Score) {
        std::cout << "It's a draw.\n";
    }

    std::string viewLoserPile;
    isGoodInput_ = false;
    while (!isGoodInput_) {
        std::cout << "Would you like to view the contents of the loser pile? (y/n)\n";
        std::cin >> userInputString_;

        if (userInputString_ != "y" && userInputString_ != "n") {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            std::cout << std::endl;
            isGoodInput_ = false;
            std::cout << "Please enter a valid input\n";
        } else { isGoodInput_ = true; }
    }
    if (userInputString_ == "y") {
        dead.printHeadToTail();
    }

    team1->deleteNodes();
    team2->deleteNodes();
}

turnResult menu::turn(LinkedList *team1, LinkedList *team2, LinkedList *dead, int round) {
    //int round = 1;
    bool stillAlive = true;
    int attackRoll = 0;

    while (true) {
        //character 1 attacks

        //round information

        /*debug
      << "Attacker type: " << team1->getNextCharacter()->getDisplayType() << "\n"
      << "Defender type: " << team2->getNextCharacter()->getDisplayType() << "\n"
      << "Defender armor: " << team2->getNextCharacter()->getArmor() << "\n"
      << "Defender strength: " << team2->getNextCharacter()->getStrengthPoints() << "\n";
      */

        //if both characters are still alive then character 1s turn begins
        if (((team1->getNextCharacter()->getStrengthPoints()) > 0) &&
            ((team2->getNextCharacter()->getStrengthPoints()) > 0)) {
            attackRoll = team1->getNextCharacter()->attackFunction();

            //prints helpful information for medusas special ability
            /*debug
            if (attackRoll == 100) {
                std::cout << "Attack dice roll: " << 12 << "\n";
                std::cout << "SPECIAL ABILITY USED: attack now = " << attackRoll << "\n";

            } else { std::cout << "Attack dice roll: " << attackRoll << "\n"; }
            */

            //calls characters 2s defense function
            int damageDealt = team2->getNextCharacter()->defenseFunction(attackRoll);

            //round information
            /*debug
            std::cout << "Total inflicted damage: " << damageDealt << "\n";
            std::cout << "Defenders updated strength: " << team2->getNextCharacter()->getStrengthPoints() << "\n";
            std::cout << "******************************************************\n";
             */
        }

        //checks if this turn killed character 2
        if ((team2->getNextCharacter()->getStrengthPoints()) <= 0) {
            std::cout << "Round " << round << ": "
                      << "Team A " << team1->getNextCharacter()->getDisplayType() << " "
                      << team1->getNextCharacter()->getCharacterName() << " vs. "
                      << "Team B " << team2->getNextCharacter()->getDisplayType() << " "
                      << team2->getNextCharacter()->getCharacterName() << ", "
                      << team1->getNextCharacter()->getCharacterName() << " won!\n";

            stillAlive = false;
            Character *temp;
            temp = team2->getNextCharacter();
            dead->addNodeToTail(&temp);
            team2->deleteFirstNode();

            return TEAM1WON;
        }

        //character 2 attacks

        //round information
        /*debug
        std::cout << "******************* ROUND " << round << " TURN 2 *******************\n"
                  << "Attacker type: " << team2->getNextCharacter()->getDisplayType() << "\n"
                  << "Defender type: " << team1->getNextCharacter()->getDisplayType() << "\n"
                  << "Defender armor: " << team1->getNextCharacter()->getArmor() << "\n"
                  << "Defender strength: " << team1->getNextCharacter()->getStrengthPoints() << "\n";
                  */

        //if both characters are still alive then character 2s turn begins
        if (((team1->getNextCharacter()->getStrengthPoints()) > 0) &&
            ((team2->getNextCharacter()->getStrengthPoints()) > 0)) {
            attackRoll = team2->getNextCharacter()->attackFunction();

            //prints helpful information for medusa's special ability
            /*debug
            if (attackRoll == 100) {
                std::cout << "Attack dice roll: " << 12 << "\n";
                std::cout << "SPECIAL ABILITY USED: attack now = " << attackRoll << "\n";
            } else { std::cout << "Attack dice roll: " << attackRoll << "\n"; }
             */

            //calls characters 1s defense function
            int damageDealt = team1->getNextCharacter()->defenseFunction(attackRoll);

            /*
            std::cout << "Total inflicted damage: " << damageDealt << "\n";
            std::cout << "Defenders updated strength: " << team1->getNextCharacter()->getStrengthPoints() << "\n";
            std::cout << "******************************************************\n\n";
             */
        }

        //checks if this turn killed character 1
        if ((team1->getNextCharacter()->getStrengthPoints()) <= 0) {
            std::cout << "Round " << round << ": "
                      << "Team A " << team1->getNextCharacter()->getDisplayType() << " "
                      << team1->getNextCharacter()->getCharacterName() << " vs. "
                      << "Team B " << team2->getNextCharacter()->getDisplayType() << " "
                      << team2->getNextCharacter()->getCharacterName() << ", "
                      << team2->getNextCharacter()->getCharacterName() << " won!\n";

            stillAlive = false;
            Character *temp;
            temp = team1->getNextCharacter();
            dead->addNodeToTail(&temp);
            team1->deleteFirstNode();

            return TEAM2WON;
        }

        //increments round for helpful output
        //round++;
    }
}