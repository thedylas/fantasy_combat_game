/*********************************************************************
 ** Program name: project3
 ** Author: David Anderson
 ** Date: 02/10/2019
 ** Description: This program is fantasy combat game
 *********************************************************************/

#ifndef PROJECT3_BARBARIAN_HPP
#define PROJECT3_BARBARIAN_HPP

#include "Character.hpp"

class Barbarian : public Character {
public:
    //constructor
    Barbarian() = default;


    //functions inherited from the parent class
    int attackFunction();


    int defenseFunction(int);

private:
    int attackNumberOfDie_ = 2;
    int attackDieSides_ = 6;
    int defenseNumberOfDie_ = 2;
    int defenseDieSides_ = 6;
    int armor_ = 0;
    int strengthPoints_ = 12;
    std::string characterName_;
};


#endif //PROJECT3_BARBARIAN_HPP
