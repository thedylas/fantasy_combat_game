/*********************************************************************
 ** Program name: project3
 ** Author: David Anderson
 ** Date: 02/10/2019
 ** Description: This program is fantasy combat game
 *********************************************************************/
#include <iostream>

#include "LinkedList.hpp"

//adds a new node to the tail and then prints the list
void LinkedList::addNodeToTail(Character **c) {
    Node *tmp = new Node;
    tmp->setCharacter(c);
    tmp->setNext(nullptr);

    if (tail_ == nullptr) {
        head_ = tmp;
        tail_ = tmp;
    } else {
        tail_->setNext(tmp);
        tmp->setPrev(tail_);
        tail_ = tmp;
    }
}

//deletes the first node in the list
void LinkedList::deleteFirstNode() {
    if (head_ == nullptr && tail_ == nullptr) {
        std::cout << "This linked list is empty.\n";
        return;
    } else if (head_->getNext() == nullptr) {
        head_ = nullptr;
        tail_ = nullptr;
    } else {
        delete head_->getPrev();
        head_ = head_->getNext();
        head_->setPrev(nullptr);
    }
}

//deletes the list from head to tail
void LinkedList::printHeadToTail() {
    if (tail_ == nullptr && head_ == nullptr) {
        std::cout << "This linked list is empty.\n";
    } else {
        Node *tmp;
        tmp = head_;
        while (tmp) { //!= nullptr) {
            std::cout << tmp->getCharacter()->getCharacterName() << " ";
            tmp = tmp->getNext();
        }
        std::cout << "\n";
    }
}


//constructor
LinkedList::LinkedList() {
    head_ = nullptr;
    tail_ = nullptr;
}


//destructor
LinkedList::~LinkedList() = default;


//deletes dynamically created nodes
void LinkedList::deleteNodes() {
    while (head_ != nullptr) {
        if (head_ == nullptr && tail_ == nullptr) {
            std::cout << "This linked list is empty.\n";
        } else if (head_->getNext() == nullptr) {
            head_ = nullptr;
            tail_ = nullptr;
        } else {
            delete head_->getNext();
            head_ = head_->getNext()->getNext();
            head_->setPrev(nullptr);
        }
    }
}


//prints the value associated with the head
void LinkedList::printHeadValue() {
    if (head_ == nullptr) {
        std::cout << "This linked list is empty.\n";
        return;
    } else {
        std::cout << "The value that head is pointing to is: " << head_->getCharacter() << "\n";
        return;
    }
}


//prints the value associated with the tail
void LinkedList::printTailValue() {
    if (tail_ == nullptr) {
        std::cout << "This linked list is empty.\n";
        return;
    } else {
        std::cout << "The value that tail is pointing to is: " << tail_->getCharacter() << "\n";
        return;
    }
}

Character *LinkedList::getNextCharacter() {
    if (head_ == nullptr) {
        return nullptr;
    } else { return head_->getCharacter(); }
}
