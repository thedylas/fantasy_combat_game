/*********************************************************************
 ** Program name: project3
 ** Author: David Anderson
 ** Date: 02/10/2019
 ** Description: This program is fantasy combat game
 *********************************************************************/

#include "Character.hpp"

//constructor
Character::Character() = default;


//destructor
Character::~Character() = default;

int Character::getArmor() {
    return armor_;
}

int Character::getStrengthPoints() {
    return strengthPoints_;
}

std::string Character::getDisplayType() {
    return displayType_;
}

std::string Character::getCharacterName() {
    return characterName_;
}

void Character::setCharacterName(std::string cn) {
    characterName_ = cn;
}

void Character::setDisplayType(std::string type) {
    displayType_ = type;
}

void Character::setArmor(int d) {
    armor_ = d;
}

void Character::setAttackNumberOfDie(int anod) {
    attackNumberOfDie_ = anod;
}

void Character::setAttackDieSides(int ads) {
    attackDieSides_ = ads;
}

void Character::setDefenseNumberOfDie(int dnod) {
    defenseNumberOfDie_ = dnod;
}

void Character::setDefenseDieSides(int dds) {
    defenseDieSides_ = dds;
}

void Character::setStrengthPoints(int sp) {
    strengthPoints_ = sp;
}


void Character::recover() {
    strengthPoints_ *= 1.5;
}

//functions that are inherited by child classes
int Character::attackFunction() {
    int attackRoll = 0;

    for (int i = 1; i <= this->attackNumberOfDie_; i++) {
        attackRoll += (rand() % this->attackDieSides_) + 1;
    }

    return attackRoll;
}

int Character::defenseFunction(int attackRoll) {
    int defenseRoll = 0;
    int damageInflicted = 0;

    for (int i = 1; i <= this->defenseNumberOfDie_; i++) {
        defenseRoll += (rand() % this->defenseDieSides_) + 1;
    }

    /*debug
    std::cout << "Defense dice roll: " << defenseRoll << "\n";
    */

    damageInflicted = attackRoll - defenseRoll - this->armor_;
    if (damageInflicted > 0 && this->strengthPoints_ > 0) {
        setStrengthPoints(((getStrengthPoints()) - damageInflicted));
    }

    return 0;
}