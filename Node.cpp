/*********************************************************************
 ** Program name: project3
 ** Author: David Anderson
 ** Date: 02/10/2019
 ** Description: This program is fantasy combat game
 *********************************************************************/

#include "Node.hpp"

//get functions
Node *Node::getNext() {
    return next_;
}

Node *Node::getPrev() {
    return prev_;
}

Character *Node::getCharacter() {
    return character_;
}


//set functions
void Node::setNext(Node *next) {
    next_ = next;
}

void Node::setPrev(Node *prev) {
    prev_ = prev;
}

void Node::setCharacter(Character **character) {
    character_ = *character;
}

//destructor
Node::~Node() {

}
