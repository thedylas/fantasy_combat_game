/*********************************************************************
 ** Program name: project3
 ** Author: David Anderson
 ** Date: 02/10/2019
 ** Description: This program is fantasy combat game
 *********************************************************************/

#ifndef CS162_LAB6_LINKEDLIST_HPP
#define CS162_LAB6_LINKEDLIST_HPP

#include "Node.hpp"
#include "Character.hpp"

class LinkedList {
public:
    //constructor
    LinkedList();

    //destructor
    ~LinkedList();


    void addNodeToTail(Character **c);


    void deleteFirstNode();


    void printHeadToTail();


    void deleteNodes();


    void printHeadValue();


    Character *getNextCharacter();


    void printTailValue();

private:
    Node *head_;       //points to first node
    Node *tail_;       //points to last node
};


#endif //CS162_LAB6_LINKEDLIST_HPP
